//
//  RealmService.swift
//  RealmToDo
//
//  Created by Эльдар on 18/5/23.
//

import Foundation
import RealmSwift

final class RealmService {
    
    static let shared = RealmService()
    
    private lazy var storage: Realm? = {
        let realm = try? Realm()
        return realm
    }()
    
    func saverUpdateObject (object: Object) throws {
        guard let storage else { return }
        storage.writeAsync {
            storage.add (object)
        }
    }
    
    func save0rUpdateObjects (objects: [Object]) throws {
        try objects.forEach({ object in
            try saverUpdateObject(object: object)
        })
    }
    
    func delete(object: Object) throws {
        guard let storage else { return }
        try storage.write({
            storage.delete(object)
        })
    }
    
    func deleteAll() throws {
        guard let storage else { return }
        try storage.write({
            storage.deleteAll()
        })
    }
    
    func fetch<T: Object>(by type: T.Type) -> [T] {
        guard let storage else { return []}
        return storage.objects(T.self).toArray()
    }
}

extension Results {
    func toArray() -> [Element] {
        .init(self)
    }
}
