//
//  RealmModuleBuilder.swift
//  RealmToDo
//
//  Created by Эльдар on 18/5/23.
//  
//

import UIKit

final class RealmModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = RealmViewController()
        let presenter = RealmPresenter()
        let interactor = RealmInteractor()
        let router = RealmRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
