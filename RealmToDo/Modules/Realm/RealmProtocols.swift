//
//  RealmProtocols.swift
//  RealmToDo
//
//  Created by Эльдар on 18/5/23.
//  
//

import Foundation

protocol RealmViewProtocol: AnyObject {
}

protocol RealmViewToPresenterProtocol: AnyObject {
}

protocol RealmInteractorProtocol: AnyObject {
}

protocol RealmInteractorToPresenterProtocol: AnyObject {
}

protocol RealmRouterProtocol: AnyObject {
}
