//
//  RealmPresenter.swift
//  RealmToDo
//
//  Created by Эльдар on 18/5/23.
//  
//

import Foundation

final class RealmPresenter {
    
    weak var view: RealmViewProtocol?
    var interactor: RealmInteractorProtocol?
    var router: RealmRouterProtocol?
    
}

extension RealmPresenter: RealmViewToPresenterProtocol {
}

extension RealmPresenter: RealmInteractorToPresenterProtocol {
}
