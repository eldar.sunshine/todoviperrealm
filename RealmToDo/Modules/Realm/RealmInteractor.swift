//
//  RealmInteractor.swift
//  RealmToDo
//
//  Created by Эльдар on 18/5/23.
//  
//

import Foundation

final class RealmInteractor {
    weak var presenter: RealmInteractorToPresenterProtocol?
}

extension RealmInteractor: RealmInteractorProtocol {
}
