//
//  RealmViewController.swift
//  RealmToDo
//
//  Created by Эльдар on 18/5/23.
//  
//

import UIKit

final class RealmViewController: UIViewController {
    
    var presenter: RealmViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    override func loadView() {
        super.loadView()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension RealmViewController: RealmViewProtocol {
}

extension RealmViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
    }
}
