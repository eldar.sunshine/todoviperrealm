//
//  AppCoordinator.swift
//  RealmToDo
//
//  Created by Эльдар on 18/5/23.
//

import Foundation
import UIKit

final class AppCoordinator {
    
    private let window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
    }

    func start() {
        openRootViewController()
    }
    
//    fileprivate func openRootViewController() {
//        window?.rootViewController = UINavigationController(rootViewController: MainModuleConfigurator.build())
//    }
    
    fileprivate func openRootViewController() {
        window?.rootViewController = UINavigationController(rootViewController: ViewController())
    }
}
